## FFTW
```
$ make
$ for((i=0; i <5;i++)); do mpirun -np $((2**$i)) ./diffusion.x > $((2**$i)); done
$ for((i=0; i <5;i++)); do cat $((2**$i))| grep 'time' | awk 'END{print $8}'; done > plot
```
### Output of Inbuilt
![](https://github.com/rjtkp/P2.8_fftw/blob/master/D1-exercise1/provided_code/C/animate.gif)
![](https://github.com/rjtkp/P2.8_fftw/blob/master/D1-exercise1/provided_code/C/diffusivity.png)
```
Nprocs    Timing
1	        0.074804
2	        0.056497
4	        0.032052
8	        0.019820
16	       0.017677
```

### Output of Manual Paralalization
![](https://github.com/rjtkp/P2.8_fftw/blob/master/D2-exercise2/provided_code/C/animate.gif)
![](https://github.com/rjtkp/P2.8_fftw/blob/master/D2-exercise2/provided_code/C/diffusivity.png)
```
Nprocs    Timing
1         0.146475
2         0.080534
4         0.059599
8         0.025420
16        0.01864
```
### Speed Up
![](https://github.com/rjtkp/P2.8_fftw/blob/master/D2-exercise2/provided_code/C/speedup.png)
