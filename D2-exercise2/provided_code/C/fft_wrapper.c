/* Assignement:
 * Here you have to modify the includes, the array sizes and the fftw calls, to
 * use the fftw-mpi
 *
 * Regarding the fftw calls. here is the substitution
 * fftw_plan_dft_3d -> fftw_mpi_plan_dft_3d
 * ftw_execute_dft  > fftw_mpi_execute_dft
 * use fftw_mpi_local_size_3d for local size of the arrays
 *
 * Created by G.P. Brandino, I. Girotto, R. Gebauer
 * Last revision: March 2016
 *
 */
#include "utilities.h"
#include <assert.h>
#include <complex.h>
#include <fftw3-mpi.h>
#include <fftw3.h>
#include <stdbool.h>
#include <stdlib.h>
#include <string.h>

double seconds() {
  /* Return the second elapsed since Epoch (00:00:00 UTC, January 1, 1970) */
  struct timeval tmp;
  double sec;
  gettimeofday(&tmp, (struct timezone *)0);
  sec = tmp.tv_sec + ((double)tmp.tv_usec) / 1000000.0;
  return sec;
}

int index_f(int i1, int i2, int i3, int n2, int n3) {

  return n3 * n2 * i1 + n3 * i2 + i3;
}

void init_fftw(fftw_dist_handler *fft, int n1, int n2, int n3, MPI_Comm comm) {

  fft->mpi_comm = comm;

  /*
     *  Allocate a distributed grid for complex FFT using aligned memory
   * allocation
     *  See details here:
     *  http://www.fftw.org/fftw3_doc/Allocating-aligned-memory-in-Fortran.html#Allocating-aligned-memory-in-Fortran
     *  HINT: initialize all global and local dimensions. Consider the first
   * dimension being multiple of the number of processes
     *
     */

  MPI_Comm_size(comm, &fft->size);
  MPI_Comm_rank(comm, &fft->rank);

  fft->n1_local = n1 / fft->size;
  fft->n1_local_offset = fft->rank * (n1 / fft->size);
  fft->local_size_grid = fft->n1_local * n2 * n3;

  printf("proc %d/%d report\n", fft->rank, fft->size);

  if (((n1 % fft->size) || (n2 % fft->size)) && !fft->rank) {

    fprintf(stdout, "\nN1 dimension must be multiple of the number of "
                    "processes. The program will be aborted...\n\n");
    MPI_Abort(comm, 1);
  }

  /* Fill the missing parts */
  fft->n1 = n1;
  fft->n2 = n2;
  fft->n3 = n3;

  /*
   * Allocate fft->fftw_data and create an FFTW plan for each 1D FFT among all
   * dimensions
   *
   */

  fft->fftw_data = fftw_alloc_complex(fft->local_size_grid);

  // the transformation has been devided into two parts
  // in a 2d and 1d   transformation

  //  2D
  fft->fftw_2d = fftw_alloc_complex(n2 * n3);
  fft->fw_2d =
      fftw_plan_dft_2d(n2, n3, fft->fftw_2d, fft->fftw_2d, FFTW_FORWARD,
                       FFTW_ESTIMATE); // for in-place transformation
  fft->bw_2d = fftw_plan_dft_2d(n2, n3, fft->fftw_2d, fft->fftw_2d,
                                FFTW_BACKWARD, FFTW_ESTIMATE);

  // 1D
  fft->fftw_1d = fftw_alloc_complex(n1);
  fft->fw_1d = fftw_plan_dft_1d(n1, fft->fftw_1d, fft->fftw_1d, FFTW_FORWARD,
                                FFTW_ESTIMATE);
  fft->bw_1d = fftw_plan_dft_1d(n1, fft->fftw_1d, fft->fftw_1d, FFTW_BACKWARD,
                                FFTW_ESTIMATE);
}

void close_fftw(fftw_dist_handler *fft) {
  fftw_destroy_plan(fft->fw_1d);
  fftw_destroy_plan(fft->fw_2d);
  // fftw_destroy_plan( fft->bw_plan_i3 );

  fftw_destroy_plan(fft->bw_1d);
  fftw_destroy_plan(fft->bw_2d);
  // fftw_destroy_plan( fft->fw_plan_i3 );

  fftw_free(fft->fftw_data);
  fftw_free(fft->fftw_1d);
  fftw_free(fft->fftw_2d);
}

/* This subroutine uses fftw to calculate 3-dimensional discrete FFTs.
 * The data in direct space is assumed to be real-valued
 * The data in reciprocal space is complex.
 * direct_to_reciprocal indicates in which direction the FFT is to be calculated
 *
 * Note that for real data in direct space (like here), we have
 * F(N-j) = conj(F(j)) where F is the array in reciprocal space.
 * Here, we do not make use of this property.
 * Also, we do not use the special (time-saving) routines of FFTW which
 * allow one to save time and memory for such real-to-complex transforms.
 *
 * f: array in direct space
 * F: array in reciprocal space
 *
 * F(k) = \sum_{l=0}^{N-1} exp(- 2 \pi I k*l/N) f(l)
 * f(l) = 1/N \sum_{k=0}^{N-1} exp(+ 2 \pi I k*l/N) F(k)
 *
 */
void fft_3d(fftw_dist_handler *fft, double *data_direct, fftw_complex *data_rec,
            bool direct_to_reciprocal) {

  int i1, i2, i3, index, start_index, end_index, i, j, k;
  int n2 = fft->n2, n3 = fft->n3, n1 = fft->n1, nprocs;

  int block_dim = fft->n1_local * fft->n1_local * n3;

  double fac = 1.0 / (n1 * n2 * n3);

  /* Allocate buffers to send and receive data */

  if (direct_to_reciprocal) {
    fftw_complex *buffer = (fftw_complex *)fftw_malloc(fft->n1_local * n2 * n3 *
                                                       sizeof(fftw_complex));

    for (i = 0; i < fft->local_size_grid; ++i)
      fft->fftw_data[i] = data_direct[i] + 0.0 * I;

    for (i1 = 0; i1 < fft->n1_local; i1++) {
      for (i2 = 0; i2 < n2; i2++) {
        for (i3 = 0; i3 < n3; i3++) {
          fft->fftw_2d[i2 * n3 + i3] =
              fft->fftw_data[i1 * n2 * n3 + i2 * n3 + i3];
        }
      }
      // 2D forward transformation
      fftw_execute_dft(fft->fw_2d, fft->fftw_2d,
                       fft->fftw_2d); // in_place fftw transformation

      for (i2 = 0; i2 < n2; i2++) {
        for (i3 = 0; i3 < n3; i3++) {
          fft->fftw_data[i1 * n2 * n3 + i2 * n3 + i3] =
              fft->fftw_2d[i2 * n3 + i3];
        }
      }
    }

    for (nprocs = 0; nprocs < fft->size; ++nprocs) {
      index = nprocs * block_dim;
      start_index = nprocs * fft->n1_local;
      for (i = 0; i < fft->n1_local; ++i) {
        end_index = (nprocs + 1) * fft->n1_local;
        for (j = start_index; j < end_index; ++j) {
          for (k = 0; k < n3; ++k) {
            buffer[index] = fft->fftw_data[i * n2 * n3 + j * n3 + k];
            index++;
          }
        }
      }
    }

    /*
      * Reorder the different data blocks to be contigous in memory.
      * The new distribution will allow to use the Alltoall function
      *
      */

    // Perform an Alltoall communication
    MPI_Alltoall(buffer, fft->n1_local * fft->n1_local * n3 * 2, MPI_DOUBLE,
                 fft->fftw_data, fft->n1_local * fft->n1_local * n3 * 2,
                 MPI_DOUBLE, fft->mpi_comm);
    // Perform an Alltoall communication

    /*
     * Reoder the different data blocks to be consistent with the initial
     * distribution.
     *
     */

    for (i2 = 0; i2 < fft->n1_local; ++i2) {
      for (i3 = 0; i3 < n3; ++i3) {
        for (i1 = 0; i1 < n1; ++i1)
          fft->fftw_1d[i1] =
              fft->fftw_data[i1 * n3 * fft->n1_local + i2 * n3 + i3];

        // 1D forward transformation
        fftw_execute_dft(fft->fw_1d, fft->fftw_1d, fft->fftw_1d);

        for (i = 0; i < n1; ++i)
          fft->fftw_data[i * fft->n1_local * n3 + i2 * n3 + i3] =
              fft->fftw_1d[i];
      }
    }

    MPI_Alltoall(fft->fftw_data, fft->n1_local * fft->n1_local * n3 * 2,
                 MPI_DOUBLE, buffer, fft->n1_local * fft->n1_local * n3 * 2,
                 MPI_DOUBLE, fft->mpi_comm);

    for (i1 = 0; i1 < fft->n1_local; ++i1) {
      index = i1 * n2 * n3;
      for (nprocs = 0; nprocs < fft->size; ++nprocs) {
        for (i2 = 0; i2 < fft->n1_local; ++i2) {
          for (i3 = 0; i3 < n3; ++i3) {
            fft->fftw_data[index] = buffer[i3 + nprocs * block_dim +
                                           i1 * fft->n1_local * n3 + i2 * n3];
            index = index + 1;
          }
        }
      }
    }

    for (i = 0; i < fft->local_size_grid; ++i) {
      data_rec[i] = fft->fftw_data[i];
    }
    fftw_free(buffer);

  } else {
    /* Implement the reverse transform */
    fftw_complex *buffer = (fftw_complex *)fftw_malloc(fft->n1_local * n2 * n3 *
                                                       sizeof(fftw_complex));
    for (i = 0; i < fft->local_size_grid; ++i)
      fft->fftw_data[i] = data_rec[i];

    for (i1 = 0; i1 < fft->n1_local; i1++) {
      for (i2 = 0; i2 < n2; i2++) {
        for (i3 = 0; i3 < n3; i3++) {
          fft->fftw_2d[i2 * n3 + i3] =
              fft->fftw_data[i1 * n2 * n3 + i2 * n3 + i3];
        }
      }
      // 2D reverse transformation
      fftw_execute_dft(fft->bw_2d, fft->fftw_2d,
                       fft->fftw_2d); // in_place fftw transformation

      for (i2 = 0; i2 < n2; i2++) {
        for (i3 = 0; i3 < n3; i3++) {
          fft->fftw_data[i1 * n2 * n3 + i2 * n3 + i3] =
              fft->fftw_2d[i2 * n3 + i3];
        }
      }
    }

    for (nprocs = 0; nprocs < fft->size; ++nprocs) {
      index = nprocs * block_dim;
      start_index = nprocs * fft->n1_local;
      for (i = 0; i < fft->n1_local; ++i) {
        end_index = (nprocs + 1) * fft->n1_local;
        for (j = start_index; j < end_index; ++j) {
          for (k = 0; k < n3; ++k) {
            buffer[index] = fft->fftw_data[i * n2 * n3 + j * n3 + k];
            index++;
          }
        }
      }
    }

    MPI_Alltoall(buffer, fft->n1_local * fft->n1_local * n3 * 2, MPI_DOUBLE,
                 fft->fftw_data, fft->n1_local * fft->n1_local * n3 * 2,
                 MPI_DOUBLE, fft->mpi_comm);
    // Perform an Alltoall communication

    /*
     * Reoder the different data blocks to be consistent with the initial
     * distribution.
     *
     */

    for (i2 = 0; i2 < fft->n1_local; ++i2) {
      for (i3 = 0; i3 < n3; ++i3) {
        for (i1 = 0; i1 < n1; ++i1)
          fft->fftw_1d[i1] =
              fft->fftw_data[i1 * n3 * fft->n1_local + i2 * n3 + i3];

        // 1D forward transformation
        fftw_execute_dft(fft->bw_1d, fft->fftw_1d,
                         fft->fftw_1d); // in-place transformation

        for (i = 0; i < n1; ++i)
          fft->fftw_data[i * fft->n1_local * n3 + i2 * n3 + i3] =
              fft->fftw_1d[i];
      }
    }

    MPI_Alltoall(fft->fftw_data, fft->n1_local * fft->n1_local * n3 * 2,
                 MPI_DOUBLE, buffer, fft->n1_local * fft->n1_local * n3 * 2,
                 MPI_DOUBLE, fft->mpi_comm);

    for (i1 = 0; i1 < fft->n1_local; ++i1) {
      index = i1 * n2 * n3;
      for (nprocs = 0; nprocs < fft->size; ++nprocs) {
        for (i2 = 0; i2 < fft->n1_local; ++i2) {
          for (i3 = 0; i3 < n3; ++i3) {
            fft->fftw_data[index] = buffer[i3 + nprocs * block_dim +
                                           i1 * fft->n1_local * n3 + i2 * n3];
            index = index + 1;
          }
        }
      }
    }

    for (i = 0; i < fft->local_size_grid; ++i) {
      data_direct[i] = creal(fft->fftw_data[i]) * fac;
    }
    fftw_free(buffer);
  }
}
// end
